package semana2ejerciciopersonacoche;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author estudiante
 */
public class Corazon {
    private int ritmo;

    //Leer ritmo es lo mismo que getRitmo
    public int getRitmo() {
        return ritmo;
    }
    //Cambiar ritmo es lo mismo que setRitmo
    public void setRitmo(int valor) {
        this.ritmo = valor;
    }

    
    @Override
    public String toString() {
        return "Corazon{" + "ritmo=" + ritmo + '}';
    }

}
