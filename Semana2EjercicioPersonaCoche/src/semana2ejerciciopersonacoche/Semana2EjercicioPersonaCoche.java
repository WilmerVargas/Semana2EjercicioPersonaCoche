/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana2ejerciciopersonacoche;

/**
 *
 * @author estudiante
 */
public class Semana2EjercicioPersonaCoche {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona persona = new Persona ("Wilmer");
        persona.emociona();
        persona.emociona();
        persona.emociona();
        persona.tranquliza();
        persona.tranquliza();
        persona.tranquliza();
       
        Motor m1 = new Motor();
        Motor m2 = new Motor();
        Motor m3 = new Motor();
        Coche c1 = new Coche(m3);
        Coche c2 = new Coche(m1);
        Coche c3 = new Coche(m2);
        c1.acelerar();
        c1.frena();
        c1.setConductor(persona);
        persona.setCoche(c1);// de esta manera se cae si no tengo el totString modificado, es decir con getNombre "conductor.getNombre".
        System.out.println(persona);
        System.out.println(c1);
    }
        
}
