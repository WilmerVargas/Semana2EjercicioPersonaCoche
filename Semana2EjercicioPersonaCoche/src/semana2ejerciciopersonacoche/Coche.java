package semana2ejerciciopersonacoche;

import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author estudiante
 */
public class Coche {

    private Persona p;
    private LinkedList<Motor> m;

    public Coche(LinkedList<Motor> m) {

        this.m = this.m;
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }

    public LinkedList<Motor> getM() {
        return m;
    }

    public void setM(LinkedList<Motor> m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "Coche{" + "p=" + p + ", m=" + m + '}';
    }

    public void asignarConductor(Persona p) {
        this.p = p;
    }

    public void encender() {

    }

    public void apagar() {

    }

    public void acelerar() {

    }

    public void frena() {

    }

    public boolean estaEncendido() {
        return true;
    }

}
