package semana2ejerciciopersonacoche;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author estudiante
 */
public class Motor {
    private int revolucionesPorMinuto;
    private boolean activo;

    public Motor(int revolucionesPorMinuto, boolean activo) {
        this.revolucionesPorMinuto = revolucionesPorMinuto;
        this.activo = activo;
    }

    public int getRevolucionesPorMinuto() {
        return revolucionesPorMinuto;
    }
    // es lo mismo que generar el CambiarRevoluciones por minuto
    public void setRevolucionesPorMinuto(int rpm) {
        this.revolucionesPorMinuto = rpm;
    }
    // es el metodo estaActivo pero en ingles
    public boolean isActivo() {
        return activo;
    }
    
    public void activo(){
        activo = true;
    }
    
    public void desactiva(){
        activo = false;
    }

    // la combinacion de los metodos activo y desactivo es el resultado del setActivo, 
    //pero por orden es mas entendible de esta manera en este caso
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Motor{" + "revolucionesPorMinuto=" + revolucionesPorMinuto + ", activo=" + activo + '}';
    }

    
   
}
