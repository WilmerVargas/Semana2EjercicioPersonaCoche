package semana2ejerciciopersonacoche;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author estudiante
 */
public class Persona {
    private String nombre;
    private Corazon corazon;
    public Persona(String nom) {
        this.nombre = nom;
        corazon = new Corazon();
        corazon.setRitmo(90);
    }

   public void viaja(){
       
   }
   
   public void emociona(){
       //optengo el ritmo actual de esta manera linea 29
       int ritmo = corazon.getRitmo();
       // lo modifico de esta manera linea 31
       corazon.setRitmo(ritmo+3);
   }
   
   public void tranquliza(){
       //optengo el ritmo actual de esta manera linea 29
       int ritmo = corazon.getRitmo();
       // lo modifico de esta manera linea 31
       corazon.setRitmo(ritmo-1);
   }
}
